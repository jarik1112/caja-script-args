const assert = require('assert');

describe('Test .env() function', function() {
  process.env.CAJA_SCRIPT_SELECTED_FILE_PATHS = './test\n./node';
  const argv = require('../index').env();
  describe('key parsing', function() {
    it('should return array', function() {
      assert.equal(true, argv.CAJA_SCRIPT_SELECTED_FILE_PATHS instanceof Array);
    });
    it('should return array of two elements', function() {
      assert.equal(argv.CAJA_SCRIPT_SELECTED_FILE_PATHS.length, 2);
    });
  });
});