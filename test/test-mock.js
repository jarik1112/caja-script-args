const assert = require('assert');

describe('Test .mock() function', function() {
  const parser = require('../index');
  
  describe('test .mock() validation', function() {
    it('should throw an Error when passed wrong keys', function() {
      assert.throws(() => {
        parser.mock({J: 10});
      });
    });

    
  });

  describe('set .mock()', function() {
    it('.env() should return same array ', function() {
      const mockData = {
        CAJA_SCRIPT_SELECTED_FILE_PATHS: [
          './test',
          './node'
        ],
      };
      parser.mock(mockData)
      assert.deepEqual(parser.env(), mockData);
    });
  });
});