const { env } = process;

const envNames = [
  'CAJA_SCRIPT_SELECTED_FILE_PATHS',
  'NAUTILUS_SCRIPT_SELECTED_FILE_PATHS',
  'CAJA_SCRIPT_SELECTED_URIS',
  'NAUTILUS_SCRIPT_SELECTED_URIS',
  'CAJA_SCRIPT_CURRENT_URI',
  'NAUTILUS_SCRIPT_CURRENT_URI',
  'CAJA_SCRIPT_WINDOW_GEOMETRY',
  'NAUTILUS_SCRIPT_WINDOW_GEOMETRY',
  'CAJA_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS',
  'NAUTILUS_SCRIPT_NEXT_PANE_SELECTED_FILE_PATHS',
  'CAJA_SCRIPT_NEXT_PANE_SELECTED_URIS',
  'NAUTILUS_SCRIPT_NEXT_PANE_SELECTED_URIS',
  'CAJA_SCRIPT_NEXT_PANE_CURRENT_URI',
  'NAUTILUS_SCRIPT_NEXT_PANE_CURRENT_URI'
];

module.exports = {
  mock(mockData) {
    const hasWrongKey = Object.keys(mockData)
      .some(key => envNames.indexOf(key) === -1);
    if(hasWrongKey) {
      throw Error('Mock object has bad key');
    }

    this._mockData = mockData;
  },
  env() {
    if (this._mockData) {
      return this._mockData;
    }
    this._result = envNames.reduce((result, key) => {
      if (env[key]) {
        if (key.indexOf('GEOMETRY') > -1 || key.indexOf('CURRENT_URI') > -1) {
          result[key] = env[key];
        } else {
          result[key] = env[key].split('\n').filter(v => Boolean(v));
        }
      }
      return result;
    }, {});
    return this._result;
  }
}